package org.bitbucket.oauthn;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.support.OAuth2ConnectionFactory;
import org.springframework.social.oauth2.AccessGrant;
import org.springframework.social.oauth2.GrantType;
import org.springframework.social.oauth2.OAuth2Operations;
import org.springframework.social.oauth2.OAuth2Parameters;

import javax.security.sasl.AuthenticationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class OAuth2Flow<T extends OAuth2ConnectionFactory> extends OAuthFlow<T> {
    public OAuth2Flow(boolean initiationRequired, T connectionFactory) {
        super(initiationRequired, connectionFactory);
    }

    @Override
    public Connection<?> attemptAuthentication(AuthenticationManager authenticationManager, HttpServletRequest request, String redirectUri) throws AuthenticationException {
        String code = request.getParameter("code");
        AccessGrant accessGrant = getConnectionFactory().getOAuthOperations().exchangeForAccess(code, redirectUri, null);
        return getConnectionFactory().createConnection(accessGrant);
    }

    @Override
    public void initiateAuthentication(HttpServletRequest request, HttpServletResponse response, String redirectUri, String state) throws IOException {
        OAuth2Parameters parameters = new OAuth2Parameters();
        parameters.setState(state);
        if (redirectUri != null) {
            parameters.setRedirectUri(redirectUri);
        }
        if (getConnectionFactory().getScope() != null) {
            parameters.setScope(getConnectionFactory().getScope());
        }

        OAuth2Operations operations = getConnectionFactory().getOAuthOperations();
        response.sendRedirect(operations.buildAuthenticateUrl(GrantType.AUTHORIZATION_CODE, parameters));
    }
}
