package org.bitbucket.oauthn;

import lombok.Getter;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.social.connect.Connection;

import javax.security.sasl.AuthenticationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @param <T> ConnectionFactory
 */
abstract class OAuthFlow<T> {
    @Getter
    private final boolean initiationRequired;
    @Getter
    private final T connectionFactory;

    public OAuthFlow(boolean initiationRequired, T connectionFactory) {
        this.initiationRequired = initiationRequired;
        this.connectionFactory = connectionFactory;
    }

    public abstract Connection<?> attemptAuthentication(AuthenticationManager authenticationManager, HttpServletRequest request, String redirectUri) throws AuthenticationException;

    public abstract void initiateAuthentication(HttpServletRequest request, HttpServletResponse response, String redirectUri, String state) throws IOException;
}
