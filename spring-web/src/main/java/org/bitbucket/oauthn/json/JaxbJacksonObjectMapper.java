package org.bitbucket.oauthn.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationIntrospector;
import org.springframework.util.ClassUtils;

public class JaxbJacksonObjectMapper extends com.fasterxml.jackson.databind.ObjectMapper {
    private static final boolean HAS_JAXB_MODULE = ClassUtils.isPresent("com.fasterxml.jackson.module.jaxb.JaxbAnnotationIntrospector", JaxbJacksonObjectMapper.class.getClassLoader());

    public JaxbJacksonObjectMapper() {
        if (HAS_JAXB_MODULE) {
            setAnnotationIntrospector(new JaxbAnnotationIntrospector(TypeFactory.defaultInstance()));
        }

        configure(JsonGenerator.Feature.QUOTE_FIELD_NAMES, true);
        configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true);

        configure(SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
        configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }
}
