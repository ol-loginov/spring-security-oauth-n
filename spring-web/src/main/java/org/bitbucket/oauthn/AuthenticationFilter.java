package org.bitbucket.oauthn;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.bitbucket.oauthn.json.JaxbJacksonObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.rememberme.AbstractRememberMeServices;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionFactory;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.support.OAuth2ConnectionFactory;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.Enumeration;

@Slf4j
public class AuthenticationFilter extends AbstractAuthenticationProcessingFilter {
    private static final String OAUTH_PARAM_STATE = "state";

    @Setter
    private JaxbJacksonObjectMapper codec = new JaxbJacksonObjectMapper();
    @Setter
    private String overrideSecureUrl;
    @Setter
    @Autowired(required = false)
    private ConnectionFactoryLocator connectionFactoryLocator;
    @Setter
    private String rememberMeParameterName;
    @Setter
    private String filterPathBase;

    public AuthenticationFilter() {
        super(new EmptyRequestMatcher());
    }

    @PostConstruct
    public void initialize() {
        Assert.state(filterPathBase != null, "filterPathBase is required to initialize filter");
        Assert.state(connectionFactoryLocator != null, "connectionFactoryLocator is required to initialize filter");

        if (rememberMeParameterName == null && getRememberMeServices() instanceof AbstractRememberMeServices) {
            rememberMeParameterName = (((AbstractRememberMeServices) getRememberMeServices()).getParameter());
        }
        if (!filterPathBase.endsWith("/")) {
            filterPathBase = filterPathBase + "/";
        }
        setRequiresAuthenticationRequestMatcher(new FilterRequestMatcher(this));
    }

    protected String resolveRequestURI(HttpServletRequest request) {
        String uri = request.getRequestURI();
        int pathParamIndex = uri.indexOf(';');
        if (pathParamIndex > 0) {
            // strip everything after the first semi-colon
            uri = uri.substring(0, pathParamIndex);
        }
        return uri;
    }

    protected String realizeProviderName(HttpServletRequest request) {
        String uri = resolveRequestURI(request);
        String uriPrefix = request.getContextPath() + filterPathBase;
        if (uri.length() <= uriPrefix.length() || !uri.startsWith(uriPrefix)) {
            return null;
        }

        String providerName = uri.substring((uriPrefix).length());
        return providerName.isEmpty() ? null : providerName;
    }

    /**
     * OAuth Authentication flow: See http://dev.twitter.com/pages/auth
     */
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws IOException {
        OAuthFlowState state = new OAuthFlowState();
        state.setProvider(realizeProviderName(request));
        if (request.getParameter(OAUTH_PARAM_STATE) != null) {
            state.decode(request.getParameter(OAUTH_PARAM_STATE), codec);
        }

        if (state.getProvider() == null) {
            throw new IllegalStateException("unknown OAuth provider");
        }

        OAuthFlow flow = selectFlow(state.getProvider(), request);
        if (flow == null) {
            throw new IllegalStateException("unknown OAuth flow");
        }

        if (flow.isInitiationRequired()) {
            Enumeration parameterNames = request.getParameterNames();
            while (parameterNames.hasMoreElements()) {
                String parameterName = (String) parameterNames.nextElement();
                state.requireParams().put(parameterName, request.getParameter(parameterName));
            }
            flow.initiateAuthentication(request, response, callbackUrlBase(request), state.encode(codec));
            return null;
        }

        Connection<?> connection = flow.attemptAuthentication(getAuthenticationManager(), request, callbackUrlBase(request));

        OAuthAuthenticationToken authToken = new OAuthAuthenticationToken(connection);
        authToken.setDetails(state);
        return getAuthenticationManager().authenticate(authToken);
    }

    private String callbackUrlBase(HttpServletRequest request) {
        if (StringUtils.hasText(overrideSecureUrl)) {
            return overrideSecureUrl;
        }
        String port = "";
        if ("http".equalsIgnoreCase(request.getScheme()) && request.getServerPort() != 80) {
            port += ":" + request.getServerPort();
        }
        return new StringBuilder(request.getScheme())
                .append("://")
                .append(request.getServerName())
                .append(port)
                .append(request.getServletPath())
                .toString();
    }

    private OAuthFlow selectFlow(String provider, HttpServletRequest request) throws IOException {
        ConnectionFactory<?> connectionFactory = connectionFactoryLocator.getConnectionFactory(provider);

        if (StringUtils.hasText(request.getParameter("code"))) {
            return new OAuth2Flow<>(false, (OAuth2ConnectionFactory) connectionFactory);
        } else if (connectionFactory instanceof OAuth2ConnectionFactory) {
            return new OAuth2Flow<>(true, (OAuth2ConnectionFactory) connectionFactory);
        } else {
            throw new IllegalStateException("Strategies other than OAuth2 were removed");
        }
    }

    static class EmptyRequestMatcher implements RequestMatcher {
        @Override
        public boolean matches(HttpServletRequest request) {
            return false;
        }
    }

    static class FilterRequestMatcher implements RequestMatcher {
        private final WeakReference<AuthenticationFilter> delegate;

        public FilterRequestMatcher(AuthenticationFilter delegate) {
            this.delegate = new WeakReference<>(delegate);
        }

        @Override
        public boolean matches(HttpServletRequest request) {
            AuthenticationFilter filter = delegate.get();
            return filter != null && filter.realizeProviderName(request) != null;
        }
    }
}
