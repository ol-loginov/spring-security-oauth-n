package org.bitbucket.oauthn;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.crypto.codec.Base64;

import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.TreeMap;

@Getter
@Setter
public class OAuthFlowState implements Serializable {
    private static final Charset ASCII = Charset.forName("ASCII");

    private String provider;
    private Map<String, String> params;

    public String encode(ObjectMapper codec) {
        try {
            return new String(Base64.encode(codec.writeValueAsString(this).getBytes()), ASCII);
        } catch (IOException e) {
            throw new IllegalStateException("encode is not available");
        }
    }

    public void decode(String content, ObjectMapper codec) {
        if (content == null) return;

        OAuthFlowState state;
        try {
            state = codec.readValue(Base64.decode(content.getBytes(ASCII)), OAuthFlowState.class);
        } catch (IOException e) {
            throw new IllegalStateException("invalid state parameter: " + content);
        }

        this.provider = state.provider;
        this.params = state.getParams();
    }

    public Map<String, String> requireParams() {
        if (params == null) {
            params = new TreeMap<>();
        }
        return params;
    }

    public String getParam(String parameter) {
        if (params == null || parameter == null) return null;
        return params.get(parameter);
    }

    public void eraseParams() {
        params = null;
    }
}
