package org.bitbucket.oauthn;

import lombok.Getter;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.social.connect.Connection;

import java.util.Collection;

public class OAuthAuthenticationToken extends AbstractAuthenticationToken {
    @Getter
    private final Object principal;

    OAuthAuthenticationToken(Connection<?> connection) {
        super(null);
        this.principal = connection;
        setAuthenticated(false);
    }

    /**
     * Created by the authentication provider after successful authentication
     *
     * @param userDetails principal
     * @param authorities authorities
     */
    OAuthAuthenticationToken(UserDetails userDetails, Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
        this.principal = userDetails;
        setAuthenticated(true);
    }

    @Override
    public Object getCredentials() {
        return null;
    }
}
