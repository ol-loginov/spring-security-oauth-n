package org.springframework.social.google.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class AccessTokenInfo {
    @JsonProperty("issued_to")
    private String issuedTo;
    @JsonProperty("audience")
    private String audience;
    @JsonProperty("user_id")
    private String userId;
    @JsonProperty("scope")
    private String scope;
    @JsonProperty("expires_in")
    private Long expiresIn;
    @JsonProperty("email")
    private String email;
    @JsonProperty("verified_email")
    private boolean verifiedEmail;
    @JsonProperty("access_type")
    private String accessType;
}
